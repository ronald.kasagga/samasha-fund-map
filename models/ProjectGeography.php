<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects_geography".
 *
 * @property string $project_geography_id
 * @property string $project_id
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $district_id
 * @property string $district_partners
 * @property integer $project_geography_status
 *
 * @property Country $country
 * @property Region $region
 * @property District $district
 * @property Project $project
 */
class ProjectGeography extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects_geography';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'country_id', 'region_id', 'district_id', 'district_partners'], 'required'],
            [['project_id', 'country_id', 'region_id', 'district_id', 'project_geography_status'], 'integer'],
            [['district_partners'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_geography_id' => 'Project Geography ID',
            'project_id' => 'Project ID',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'district_id' => 'District ID',
            'district_partners' => 'District Partners',
            'project_geography_status' => 'Project Geography Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['district_id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }
}
