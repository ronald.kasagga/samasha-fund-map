<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "support_budget_expenses".
 *
 * @property string $support_budget_expense_id
 * @property string $project_id
 * @property integer $cost_category_id
 * @property string $cost
 * @property string $other_cost_category_name
 * @property string $not_spent_reason
 * @property string $support_budget_expense_date
 * @property integer $support_budget_expense_status
 *
 * @property Project $project
 * @property CostCategory $costCategory
 */
class SupportBudgetExpense extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'support_budget_expenses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'cost_category_id', 'cost', 'other_cost_category_name', 'not_spent_reason'], 'required'],
            [['project_id', 'cost_category_id', 'cost', 'support_budget_expense_status'], 'integer'],
            [['not_spent_reason'], 'string'],
            [['support_budget_expense_date'], 'safe'],
            [['other_cost_category_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'support_budget_expense_id' => 'Support Budget Expense ID',
            'project_id' => 'Project ID',
            'cost_category_id' => 'Cost Category ID',
            'cost' => 'Cost',
            'other_cost_category_name' => 'Other Cost Category Name',
            'not_spent_reason' => 'Not Spent Reason',
            'support_budget_expense_date' => 'Support Budget Expense Date',
            'support_budget_expense_status' => 'Support Budget Expense Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCostCategory()
    {
        return $this->hasOne(CostCategory::className(), ['cost_category_id' => 'cost_category_id']);
    }
}
