<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "financing_sources".
 *
 * @property integer $financing_source_id
 * @property string $financing_source
 * @property string $added_by
 * @property integer $financing_source_status
 *
 * @property Project[] $projects
 */
class FinancingSource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'financing_sources';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['financing_source'], 'required'],
            [['added_by', 'financing_source_status'], 'integer'],
            [['financing_source'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'financing_source_id' => 'Financing Source ID',
            'financing_source' => 'Financing Source',
            'added_by' => 'Added By',
            'financing_source_status' => 'Financing Source Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['financing_source_id' => 'financing_source_id']);
    }
}
