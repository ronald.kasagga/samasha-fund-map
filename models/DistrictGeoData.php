<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "district".
 *
 * @property string $id
 * @property string $name
 * @property string $region
 * @property string $subregion
 * @property string $latitude
 * @property string $longitude
 * @property string $woe_name
 * @property string $hc_key
 * @property string $postal_code
 * @property string $geometry
 * @property string $added_on
 * @property string $updated_on
 *
 * @property District $district
 */
class DistrictGeoData extends \yii\db\ActiveRecord
{
    public static $importCount;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'district_geog_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'latitude', 'longitude', 'hc_key'], 'required'],
            [['geometry'], 'string'],
            [['added_on', 'updated_on'], 'safe'],
            [['id', 'name', 'region', 'subregion', 'latitude', 'longitude', 'woe_name', 'hc_key', 'postal_code'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'region' => 'Region',
            'subregion' => 'Sub Region',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'woe_name' => 'woe-name',
            'hc_key' => 'hc-key',
            'postal_code' => 'Postal Code',
            'geometry' => 'Geometry',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
        ];
    }

    /**
     * @param object $JSONData a json object of country district data typically from http://code.highcharts.com/mapdata/countries/ug/ug-all.geo.json
     */
    public static function import($JSONData){
        self::$importCount = 0;
        foreach($JSONData->features as $feature){
            $imported = self::createDistrict($feature->id, $feature->properties, json_encode($feature->geometry->coordinates));
            if($imported)
                self::$importCount++;
        }
    }

    private static function createDistrict($id, $properties, $geometry)
    {
        $district = new District();
        $district->id = $id;
        $district->name = $properties->name;
        $district->latitude = $properties->latitude;
        $district->longitude = $properties->longitude;
        $district->region = $properties->region;
        $district->subregion= $properties->subregion;
        $district->postal_code= $properties->{'postal-code'};
        $district->woe_name= $properties->{'woe-name'};
        $district->hc_key = $properties->{'hc-key'};
        $district->geometry = $geometry;
        $district->save();
        return $district;
    }

    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['district'=>'name']);
    }

}
