<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currencies".
 *
 * @property integer $currency_id
 * @property string $currency_abbreviation
 * @property string $currency_full
 *
 * @property SupportBudget[] $supportBudgets
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currencies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_abbreviation', 'currency_full'], 'required'],
            [['currency_abbreviation'], 'string', 'max' => 3],
            [['currency_full'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id' => 'Currency ID',
            'currency_abbreviation' => 'Currency Abbreviation',
            'currency_full' => 'Currency Full',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportBudget()
    {
        return $this->hasMany(SupportBudget::className(), ['currency_id' => 'currency_id']);
    }
}
