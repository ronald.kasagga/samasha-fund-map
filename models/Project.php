<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property string $project_id
 * @property string $organization_id
 * @property string $project_name
 * @property string $project_description
 * @property integer $financing_source_id
 * @property string $financing_agent
 * @property string $implementing_agent
 * @property string $project_entry_date
 * @property integer $project_status
 *
 * @property Activity[] $activities
 * @property Organization $organization
 * @property Organization $financingAgent
 * @property Organization $implementingAgent
 * @property FinancingSource $financingSource
 * @property ProjectGeography[] $projectsGeographies
 * @property ProjectGeography $projectsGeography
 * @property SupportBudgetExpense[] $supportBudgetExpenses
 * @property SupportBudget[] $supportBudgets
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'project_name', 'project_description', 'financing_source_id', 'financing_agent', 'implementing_agent'], 'required'],
            [['organization_id', 'financing_source_id', 'financing_agent', 'implementing_agent', 'project_status'], 'integer'],
            [['project_description'], 'string'],
            [['project_entry_date'], 'safe'],
            [['project_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'organization_id' => 'Organization',
            'project_name' => 'Project Name',
            'project_description' => 'Project Description',
            'financing_source_id' => 'Financing Source',
            'financing_agent' => 'Financing Agent',
            'implementing_agent' => 'Implementing Agent',
            'project_entry_date' => 'Project Entry Date',
            'project_status' => 'Project Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['organization_id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancingAgent()
    {
        return $this->hasOne(Organization::className(), ['organization_id' => 'financing_agent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImplementingAgent()
    {
        return $this->hasOne(Organization::className(), ['organization_id' => 'implementing_agent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancingSource()
    {
        return $this->hasOne(FinancingSource::className(), ['financing_source_id' => 'financing_source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectsGeographies()
    {
        return $this->hasMany(ProjectGeography::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectsGeography()
    {
        return $this->hasOne(ProjectGeography::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportBudgetExpenses()
    {
        return $this->hasMany(SupportBudgetExpense::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportBudgets()
    {
        return $this->hasMany(SupportBudget::className(), ['project_id' => 'project_id']);
    }
}
