<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activities".
 *
 * @property string $activity_id
 * @property string $project_id
 * @property integer $support_type_id
 * @property integer $support_sub_type_id
 * @property string $activities_date
 * @property integer $activity_status
 *
 * @property Project $project
 * @property SupportType $supportType
 * @property SupportSubType $supportSubType
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'support_type_id', 'support_sub_type_id'], 'required'],
            [['project_id', 'support_type_id', 'support_sub_type_id', 'activity_status'], 'integer'],
            [['activities_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activity_id' => 'Activity ID',
            'project_id' => 'Project ID',
            'support_type_id' => 'Support Type ID',
            'support_sub_type_id' => 'Support Sub Type ID',
            'activities_date' => 'Activities Date',
            'activity_status' => 'Activity Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportType()
    {
        return $this->hasOne(SupportType::className(), ['support_type_id' => 'support_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportSubType()
    {
        return $this->hasOne(SupportSubType::className(), ['support_sub_type_id' => 'support_sub_type_id']);
    }
}
