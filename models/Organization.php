<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organizations".
 *
 * @property string $organization_id
 * @property string $account_id
 * @property string $organization_name
 * @property integer $organization_type_id
 * @property integer $fiscal_year_start_month
 * @property integer $pooled_funds
 * @property integer $mou_with_moh
 * @property string $district_work_start
 * @property string $authority_id
 * @property string $other_authority
 * @property string $contact_name
 * @property string $mobile_phone_number
 * @property string $office_phone_number
 * @property string $email_address
 * @property string $data_source
 *
 * @property Account $account
 * @property OrganizationType $organizationType
 * @property Authority $authority
 * @property Project[] $projects
 * @property Project[] $financedProjects
 * @property Project[] $implementedProjects
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organizations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'organization_name', 'organization_type_id', 'fiscal_year_start_month', 'pooled_funds', 'mou_with_moh', 'authority_id', 'other_authority', 'contact_name', 'mobile_phone_number', 'office_phone_number', 'email_address', 'data_source'], 'required'],
            [['account_id', 'organization_type_id', 'fiscal_year_start_month', 'pooled_funds', 'mou_with_moh', 'authority_id'], 'integer'],
            [['district_work_start'], 'safe'],
            [['organization_name', 'other_authority', 'mobile_phone_number', 'office_phone_number', 'data_source'], 'string', 'max' => 255],
            [['contact_name'], 'string', 'max' => 25],
            [['email_address'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'organization_id' => 'Organization ID',
            'account_id' => 'Account ID',
            'organization_name' => 'Organization Name',
            'organization_type_id' => 'Organization Type ID',
            'fiscal_year_start_month' => 'Fiscal Year Start Month',
            'pooled_funds' => 'Pooled Funds',
            'mou_with_moh' => 'Mou With Moh',
            'district_work_start' => 'District Work Start',
            'authority_id' => 'Authority ID',
            'other_authority' => 'Other Authority',
            'contact_name' => 'Contact Name',
            'mobile_phone_number' => 'Mobile Phone Number',
            'office_phone_number' => 'Office Phone Number',
            'email_address' => 'Email Address',
            'data_source' => 'Data Source',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationType()
    {
        return $this->hasOne(OrganizationType::className(), ['organization_type_id' => 'organization_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthority()
    {
        return $this->hasOne(Authority::className(), ['authority_id' => 'authority_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['organization_id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancedProjects()
    {
        return $this->hasMany(Project::className(), ['financing_agent' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImplementedProjects()
    {
        return $this->hasMany(Project::className(), ['implementing_agent' => 'organization_id']);
    }
}
