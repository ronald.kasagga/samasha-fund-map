<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "support_budgets".
 *
 * @property string $support_budget_id
 * @property string $project_id
 * @property integer $currency_id
 * @property string $total_budget
 * @property integer $exchange_rate
 * @property string $local_currency_ammount
 * @property string $money_not_spent
 * @property string $support_budget_date
 *
 * @property Project $project
 * @property Currency $currency
 */
class SupportBudget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'support_budgets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'currency_id', 'total_budget', 'local_currency_ammount', 'money_not_spent'], 'required'],
            [['project_id', 'currency_id', 'total_budget', 'exchange_rate', 'local_currency_ammount', 'money_not_spent'], 'integer'],
            [['support_budget_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'support_budget_id' => 'Support Budget ID',
            'project_id' => 'Project ID',
            'currency_id' => 'Currency ID',
            'total_budget' => 'Total Budget',
            'exchange_rate' => 'Exchange Rate',
            'local_currency_ammount' => 'Local Currency Ammount',
            'money_not_spent' => 'Money Not Spent',
            'support_budget_date' => 'Support Budget Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }
}
