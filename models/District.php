<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "districts".
 *
 * @property integer $district_id
 * @property integer $region_id
 * @property string $district
 *
 * @property Region $region
 * @property ProjectGeography[] $projectsGeographies
 * @property DistrictGeoData $mapData
 */
class District extends \yii\db\ActiveRecord
{
    static $_maxProjectCount;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'districts';
    }

    public static function getMaxProjectCount()
    {
        if(self::$_maxProjectCount == null){
            $allDistrictIds = implode(',',self::getAllDistrictsIds());
            $stats = Yii::$app->db->createCommand("select (count(DISTINCT project_id) + (select count(project_id) from projects_geography where district_id in($allDistrictIds))) as projects from projects_geography WHERE district_id not in ($allDistrictIds) GROUP BY district_id;")->queryAll();
            $data = [];
            foreach($stats as $stat)
                $data[] = $stat['projects'];
            self::$_maxProjectCount = max($data);
        }
        return self::$_maxProjectCount;
    }

    public static function projectsInDistrict($district_id)
    {
        return ArrayHelper::map(ProjectGeography::find()->where(['district_id'=>ArrayHelper::merge([$district_id], self::getAllDistrictsIds())])->all(), 'project_geography_id', 'project_id');
    }

    public static function getAllDistrictsIds()
    {
        return [1,2];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'district'], 'required'],
            [['region_id'], 'integer'],
            [['district'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'district_id' => 'District ID',
            'region_id' => 'Region ID',
            'district' => 'District',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectGeographies()
    {
        return $this->hasMany(ProjectGeography::className(), ['district_id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapData()
    {
        return $this->hasOne(DistrictGeoData::className(), ['name' => 'district']);
    }

    public function getProjectCount(){
        return ProjectGeography::find()->where(['district_id'=>ArrayHelper::merge([$this->district_id], self::getAllDistrictsIds())])->count();
    }

    public static function getProjectsMapData()
    {
        $allDistrictIds = implode(',',self::getAllDistrictsIds());
        $districts = District::find()->where("district_id not in ($allDistrictIds)")->all();
        $data = [];
        if(!empty($districts)){
            foreach($districts as $district)
                $data[] = $district->projectMapArray;
        }
        return $data;
    }

    public function getProjectMapArray(){
        return [
            'id'=>$this->district_id,
            'name'=>$this->district,
            'hc-key'=>isset($this->mapData)?$this->mapData->hc_key:null,
            'value'=>$this->projectCount,
            'url'=>Url::to(['/projects/in-district', 'id'=>$this->district_id]),
        ];
    }
}
