<?php

namespace app\models\search;

use app\models\District;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project as ProjectModel;

/**
 * Project represents the model behind the search form about `app\models\Project`.
 */
class Project extends ProjectModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'organization_id', 'financing_source_id', 'financing_agent', 'implementing_agent', 'project_status'], 'integer'],
            [['project_name', 'project_description', 'project_entry_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'project_id' => $this->project_id,
            'organization_id' => $this->organization_id,
            'financing_source_id' => $this->financing_source_id,
            'financing_agent' => $this->financing_agent,
            'implementing_agent' => $this->implementing_agent,
            'project_entry_date' => $this->project_entry_date,
            'project_status' => $this->project_status,
        ]);

        $query->andFilterWhere(['like', 'project_name', $this->project_name])
            ->andFilterWhere(['like', 'project_description', $this->project_description]);

        return $dataProvider;
    }

    public function searchDistrict($params, $district_id)
    {
        $query = ProjectModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'project_id' => $this->project_id,
            'organization_id' => $this->organization_id,
            'financing_source_id' => $this->financing_source_id,
            'financing_agent' => $this->financing_agent,
            'implementing_agent' => $this->implementing_agent,
            'project_entry_date' => $this->project_entry_date,
            'project_status' => $this->project_status,
        ]);

        if(!in_array($district_id, District::getAllDistrictsIds()))
            $query->onCondition(['project_id'=>District::projectsInDistrict($district_id)]);

        $query->andFilterWhere(['like', 'project_name', $this->project_name])
            ->andFilterWhere(['like', 'project_description', $this->project_description]);

        return $dataProvider;
    }
}
