<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/27/2015
 * Time: 9:08 PM
 */

namespace app\helpers;

class DateHelper
{

    public static function formatDate($date, $format = 'D d F, Y h:i a')
    {
        return date($format, strtotime($date));
    }
}