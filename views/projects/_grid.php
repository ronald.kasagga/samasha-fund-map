<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/26/2015
 * Time: 3:40 PM
 * @var $this yii\web\View */
/* @var $searchModel app\models\search\Project */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $organisations array */
/* @var $financiers array */

use app\models\Project;
use yii\grid\GridView;
use yii\helpers\Html;
$dropDownOptions = ['class'=>'col-lg-12 select', 'prompt'=>'Any']?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //'project_id',
        ['attribute'=>'project_name', 'label'=>'Project', 'format'=>'raw', 'value'=>function(Project $data){
            return Html::a($data->project_name, ['view', 'id'=>$data->project_id]);
        }],
        [
            'attribute'=>'organization_id', 'value'=>'organization.organization_name',
            'filter'=>Html::activeDropDownList($searchModel, 'organization_id', $organisations, $dropDownOptions)
        ],
        //'project_description:ntext',
        [
            'attribute'=>'financing_source_id', 'value'=>'financingSource.financing_source',
            'filter'=>Html::activeDropDownList($searchModel, 'financing_source_id', $financiers, $dropDownOptions)
        ],
        // 'financing_agent',
        // 'implementing_agent',
        // 'project_entry_date',
        // 'project_status',

        //['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>