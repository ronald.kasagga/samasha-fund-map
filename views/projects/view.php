<?php

use app\helpers\DateHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->project_name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'project_id',
            ['attribute'=>'organization_id', 'value'=>$model->organization->organization_name],
            'project_name',
            'project_description:ntext',
            ['attribute'=>'financing_source_id', 'value'=>$model->financingSource->financing_source],
            ['attribute'=>'financing_agent', 'value'=>$model->financingAgent->organization_name],
            ['attribute'=>'implementing_agent', 'value'=>$model->implementingAgent->organization_name],
            ['attribute'=>'project_entry_date', 'value'=>DateHelper::formatDate($model->project_entry_date)],
            'project_status',
            ['label'=>'District', 'value'=>$model->projectsGeography->district->district],
        ],
    ]) ?>

</div>
