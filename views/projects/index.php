<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Project */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $organisations array */
/* @var $financiers array */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h3>
        <?= Html::encode($this->title) ?>
        <?= Html::a('View by District', ['/projects/in-district', 'id'=>1], ['class'=>'btn btn-default pull-right']) ?>

    </h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= Yii::$app->controller->renderPartial('//projects/_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'organisations'=>$organisations,
        'financiers'=>$financiers,
    ]) ?>

</div>
