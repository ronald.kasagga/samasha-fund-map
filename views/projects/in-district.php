<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Project */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $organisations array */
/* @var $financiers array */
/* @var $district \app\models\District */

$this->title = 'Projects in ' . $district->district;
$this->params['breadcrumbs'][] = ['label'=>'Projects', 'url'=>['/projects']];
$this->params['breadcrumbs'][] = 'in ' . $district->district;
?>
<div class="project-in-district">
        <div class="form-group form-inline row col-md-12">
            <label id="district-select-label" class="control-label pull-left">Projects in </label>
            <?= Html::dropDownList('id', [$district->district_id], $districts, [
                'class'=>'form-control col-md-2', 'id'=>'district-select'
            ])?>
        </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= Yii::$app->controller->renderPartial('//projects/_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'organisations'=>$organisations,
        'financiers'=>$financiers,
    ]) ?>

</div>

<style>
    #district-select-label{
        padding-top: 11px
    }
    #district-select, #district-select-label{
        font-weight: 500;
    }
    #district-select option, #district-select,#district-select-label{
        font-size: 21px;
    }
    #district-select{
        height:50px;
        border:none;
        padding-left: 2px;
    }
</style>
