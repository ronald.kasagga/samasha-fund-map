<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/26/15
 * Time: 11:59 AM
 */

use yii\helpers\Url; ?>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/jquery/jquery-ui-1.10.3.custom.min.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/jquery/jquery-migrate-1.2.1.min.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/jquery/globalize.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/other/excanvas.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/other/jquery.mousewheel.min.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/bootstrap/bootstrap.min.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/jflot/jquery.flot.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/jflot/jquery.flot.stack.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/jflot/jquery.flot.pie.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/jflot/jquery.flot.resize.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/epiechart/jquery.easy-pie-chart.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/sparklines/jquery.sparkline.min.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>

<script type='text/javascript' src="<?=Url::base()?>/js/plugins/uniform/jquery.uniform.min.js"></script>

<script type='text/javascript' src="<?=Url::base()?>/js/plugins/fullcalendar/fullcalendar.min.js"></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/shbrush/XRegExp.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/shbrush/shCore.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/shbrush/shBrushXml.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/shbrush/shBrushJScript.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/shbrush/shBrushCss.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins/select/select2.min.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/plugins.js'></script>
<script type='text/javascript' src='<?=Url::base()?>/js/charts.js'></script>

<script type='text/javascript' src='<?=Url::base()?>/js/actions.js'></script>