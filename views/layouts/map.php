<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/7/2015
 * Time: 7:23 PM
 */
use app\models\District;
use yii\helpers\Url; ?>
<script src="http://code.highcharts.com/maps/highmaps.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/ug/ug-all.js"></script>

<script>
    $(function () {

        $.ajax({
            url: '<?=Url::toRoute('/map/json')?>',
            type: 'get',
            dataType: 'json',
            cache: false,
            success: initialiseMap,
            async:true,
        });

    });

    function initialiseMap(data){
        $('#map-ug').innerHTML='';

        $('#map-ug').highcharts('Map', {

            title : {
                text : 'Projects in Districts'
            },

            subtitle : {
                text : 'Click a district to see projects under it'
            },

            mapNavigation: {
                enabled: true,
                buttonOptions: {
                    verticalAlign: 'bottom'
                }
            },

            colorAxis: {
                min: 0,
                max: <?=District::getMaxProjectCount()?>
            },

            series : [{
                data : data,
                mapData: Highcharts.maps['countries/ug/ug-all'],
                joinBy: 'hc-key',
                name: 'Projects in:',
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                point: {
                    events:{
                        click: function (event) {
                            window.location = this.url;
                        }
                    }
                }
            }]
        });
    }

</script>
