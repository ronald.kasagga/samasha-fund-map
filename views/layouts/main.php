<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) . ' - '. Yii::$app->name ?></title>
    <?php $this->head() ?>
    <?=Yii::$app->controller->renderPartial('//layouts/css')?>

</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Map', 'url' => ['/'], 'active'=>in_array(Yii::$app->controller->route, ['site/index', 'site/map'])],
                    ['label' => 'Projects', 'url' => ['/projects'], 'active'=>(Yii::$app->controller->id == 'projects')],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Login', 'url' => Yii::$app->user->loginUrl] :
                        [
                            'label' => 'Hello ' . Yii::$app->user->identity->username,
                            'items'=> [
                                ['label'=>'Log Out', 'url' => ['/site/logout'],'linkOptions' => ['data-method' => 'post']]
                            ],
                        ],
                ],
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy;
                <?=Html::a(Yii::$app->params['app-vendor'], Yii::$app->params['app-vendor-website'])?> <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
<?=Yii::$app->controller->renderPartial('//layouts/scripts')?>
<?=(Yii::$app->controller->route == 'site/map')?Yii::$app->controller->renderPartial('//layouts/map'):''?>
<script>
    $('#district-select').on('change', function(){
        window.location = '<?=Url::toRoute('/projects/in-district')?>?id=' + this.value;
    });
</script>
</body>
</html>
<?php $this->endPage() ?>
