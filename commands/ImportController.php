<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 8/10/2015
 * Time: 5:39 PM
 */

namespace app\commands;


use app\models\District;
use app\models\DistrictGeoData;
use yii\console\Controller;

class ImportController extends Controller
{
    /**
     * @param string $file path to file containing ug geo data
     */
    public function actionDistrictMapData($file = null)
    {
        $filePath = ($file)?$file:\Yii::getAlias('@app/web/js/maps/data/ug-all.geo.json');
        if(!file_exists($filePath))
            echo "file '$filePath' does not exist";
        else{
            echo "\nStarting import from : " . $filePath;
            $jsonString = file_get_contents($filePath);
            DistrictGeoData::import(json_decode($jsonString));
            echo "\nFinished importing " . DistrictGeoData::$importCount.' districts';
        }
    }
}