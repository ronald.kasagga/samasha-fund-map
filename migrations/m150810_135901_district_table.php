<?php

use yii\db\Schema;
use yii\db\Migration;

class m150810_135901_district_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%district}}',[
            'id'=>Schema::TYPE_STRING . ' not null',
            'name'=>Schema::TYPE_STRING . ' not null',
            'region'=>Schema::TYPE_STRING,
            'subregion'=>Schema::TYPE_STRING,
            'latitude'=>Schema::TYPE_STRING . ' not null',
            'longitude'=>Schema::TYPE_STRING . ' not null',
            'woe_name'=>Schema::TYPE_STRING,
            'hc_key'=>Schema::TYPE_STRING . ' not null',
            'postal_code'=>Schema::TYPE_STRING,
            'geometry'=>Schema::TYPE_TEXT,
            'added_on'=>Schema::TYPE_TIMESTAMP . ' not null default current_timestamp',
            'updated_on'=>Schema::TYPE_DATETIME. ' null on update current_timestamp',
        ]);

        $this->createIndex('district_unique_id', '{{%district}}', 'id', true);
        $this->createIndex('district_unique_name', '{{%district}}', 'name', true);
    }

    public function safeDown()
    {
        $this->dropTable('{{%district}}');
    }
}
