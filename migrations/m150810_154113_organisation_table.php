<?php

use yii\db\Schema;
use yii\db\Migration;

class m150810_154113_organisation_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%organisation}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' not null',
            'money_allocated'=>Schema::TYPE_STRING . ' not null',
            'district_id'=>Schema::TYPE_STRING . ' not null',
            'added_on'=>Schema::TYPE_TIMESTAMP . ' not null default current_timestamp',
            'updated_on'=>Schema::TYPE_DATETIME. ' null on update current_timestamp',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%organisation}}');
    }
}
