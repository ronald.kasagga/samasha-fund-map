<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_065255_refactor_tables extends Migration
{
    public function safeUp()
    {
        $this->dropTable('{{%organisation}}');
        $this->renameTable('{{%district}}', '{{%district_geog_data}}');
    }

    public function safeDown()
    {
        $this->renameTable('{{%district_geog_data}}', '{{%district}}');
        $this->createTable('{{%organisation}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' not null',
            'money_allocated'=>Schema::TYPE_STRING . ' not null',
            'district_id'=>Schema::TYPE_STRING . ' not null',
            'added_on'=>Schema::TYPE_TIMESTAMP . ' not null default current_timestamp',
            'updated_on'=>Schema::TYPE_DATETIME. ' null on update current_timestamp',
        ]);
    }
}
