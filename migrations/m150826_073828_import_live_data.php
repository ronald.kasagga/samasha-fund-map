<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_073828_import_live_data extends Migration
{
    public function safeUp()
    {
        $script = file_get_contents(Yii::getAlias('@app/data/samasha_rmnch.sql'));
        $this->execute($script);
    }

    public function safeDown()
    {
        echo "m150826_073828_import_live_data cannot be reverted.\n";

        return false;
    }
}
