-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 19, 2015 at 03:29 AM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `samasha_rmnch`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `account_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `account_email` varchar(100) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `password` varchar(128) NOT NULL,
  `level` tinyint(3) unsigned NOT NULL COMMENT '1 - Super Admin, 2 - RMNCH Admin, 3 - Reseverved For future Use, 4 - Organization',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 - No yet activated,  1 - Activated, 2 - Deactivated, 3 - Deleted',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`account_id`, `account_email`, `user_name`, `password`, `level`, `status`) VALUES
(1, 'naj.naj001@gmail.com', 'admin', '3f858fce4c88109b42f348f4cff530172f8b43698a6c6f68e0d7590eab063744', 1, 1),
(2, 'mmuwonge@samasha.org', 'samasha', '4ebd1b47bc75eaa89d2a057449f40b7f22e65cdb794d88f6477758e9bee30152', 2, 1),
(3, 'casiimwe@samasha.org', 'casiimwe', 'af4ddaf2633f61ac5248242752009fc5690e961525db906e7593572de4a26e8b', 4, 1),
(4, 'nbhardwaj@unicef.org', 'unicef', 'ee86742e19001d752c68811afc233a5e6c2c7c530c4e7ba93a46d61e6bba09d3', 4, 1),
(5, 'lkabunga@clintonhealthacccess.org', 'chaiecm', '9fd49ed700e8098abcca9aed079c6f8bc9bebee6d245231f78717303196be9f9', 4, 1),
(6, 'caruho@heps.or.ug', 'heps', 'af4ddaf2633f61ac5248242752009fc5690e961525db906e7593572de4a26e8b', 4, 1),
(7, 'casiimwe@samasha.org', 'corney', 'af4ddaf2633f61ac5248242752009fc5690e961525db906e7593572de4a26e8b', 4, 1),
(8, 'drkisambu@gmail.com', 'kettiie', '1e127432c96ff72c65ef10442ecc6fdf8e2ea923768b55d24b3e629d0f45d5ad', 4, 1),
(9, 'p.lochoro@cuamm.org', 'plochoro', 'b3c6fb0da0c8a16502767b6360315ed0a5e5803db948b998eb73024344c6860a', 4, 1),
(10, 'emily.katarikawe@jhpiego.org', 'jhpiego', 'fc9a934f097f56da5620226f795fda56087f2bd6df721d8f083262183f7f63a5', 4, 1),
(11, 'casiimwe@samasha.org', 'path', 'af4ddaf2633f61ac5248242752009fc5690e961525db906e7593572de4a26e8b', 4, 1),
(12, 'casiimwe@samasha.org', 'sida', '858bf25e88b323d1896cfa9bd1627997af2946cd5fccc7ba90cc1dbd543c4489', 4, 1),
(13, 'casiimwe@samasha.org', 'tomaliti', 'af4ddaf2633f61ac5248242752009fc5690e961525db906e7593572de4a26e8b', 4, 1),
(14, 'mmuwonge@samasha.org', 'samashamf', '45db166e246acfa745ea17aa43c0061dc78644f07a93299d65870bed65c5c648', 4, 1),
(15, 'mmukirane@eachq.org', 'mukiranem', '02c9a3d59f14715c961ad59c2a6276109bccdd9c2212f81b29f7f8971106b4e5', 4, 1),
(16, 'balidawafrank@healthchild.org.ug', 'balidawafrank', '36f3d364fb626851298e83e1127691aaacbfff3d49fb1df23d8b5809902f9be7', 4, 1),
(17, 'nalikka@unfpa.org', 'nalikka', 'aac9b9f0b7c7cd5ddeb1bac62731355e90e91ca9119ca82e10d79319a0c8332e', 4, 1),
(18, 'jbukenya@musph.ac.ug', 'jbukenya', '04a16f5ae5be4475c3609423fa16c13a811c81399a779f298cec4ae2c275c146', 4, 1),
(19, 'jemera@gmail.com', 'uhsc2015', '58a7dcf58f49144b0505bcbd2bac8b428e4b72ff3663b04ddb75522525cad6bb', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `activity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `support_type_id` tinyint(1) unsigned NOT NULL,
  `support_sub_type_id` smallint(3) unsigned NOT NULL,
  `activities_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activity_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 - on, 0 - off',
  PRIMARY KEY (`activity_id`),
  KEY `support_type_id` (`support_type_id`,`support_sub_type_id`),
  KEY `project_id` (`project_id`),
  KEY `support_sub_type_id` (`support_sub_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`activity_id`, `project_id`, `support_type_id`, `support_sub_type_id`, `activities_date`, `activity_status`) VALUES
(1, 1, 7, 50, '2015-04-09 09:44:43', 1),
(2, 3, 7, 50, '2015-04-15 12:41:09', 1),
(3, 4, 7, 50, '2015-04-15 12:59:22', 1),
(4, 5, 4, 27, '2015-04-15 13:10:28', 1),
(5, 7, 3, 22, '2015-04-29 08:41:57', 1),
(6, 7, 3, 18, '2015-04-29 08:55:33', 0),
(7, 7, 3, 20, '2015-04-29 08:56:08', 0),
(8, 7, 3, 19, '2015-04-29 08:56:17', 0),
(9, 8, 3, 18, '2015-04-29 09:00:34', 1),
(10, 8, 3, 20, '2015-04-29 09:00:45', 1),
(11, 9, 7, 50, '2015-04-29 12:38:32', 1),
(12, 13, 5, 37, '2015-04-30 06:26:11', 1),
(13, 10, 5, 33, '2015-04-30 06:44:15', 1),
(14, 11, 5, 33, '2015-04-30 07:21:20', 1),
(15, 12, 7, 50, '2015-04-30 07:40:54', 1),
(16, 6, 7, 50, '2015-04-30 07:50:18', 0),
(17, 6, 7, 50, '2015-04-30 07:56:44', 1),
(18, 14, 7, 50, '2015-04-30 08:01:12', 1),
(19, 15, 7, 50, '2015-04-30 10:15:19', 1),
(20, 17, 7, 50, '2015-04-30 12:43:54', 1),
(21, 19, 4, 29, '2015-05-07 08:20:44', 1),
(22, 19, 4, 27, '2015-05-07 08:21:17', 1),
(23, 20, 4, 29, '2015-05-07 08:35:28', 1),
(24, 21, 4, 26, '2015-05-07 10:12:22', 1),
(25, 22, 4, 27, '2015-05-13 09:47:18', 1),
(26, 23, 1, 2, '2015-06-27 08:14:04', 1),
(27, 24, 1, 2, '2015-07-01 13:06:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE IF NOT EXISTS `authorities` (
  `authority_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) NOT NULL,
  `system_added` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - No, 1 - Yes. Was it system added or added by the user',
  PRIMARY KEY (`authority_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` (`authority_id`, `authority`, `system_added`) VALUES
(1, 'Ministry of Health', 1),
(2, 'Other Authorities', 1),
(3, 'District Local Government', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cost_categories`
--

CREATE TABLE IF NOT EXISTS `cost_categories` (
  `cost_category_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `cost_category` varchar(80) NOT NULL,
  PRIMARY KEY (`cost_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `cost_categories`
--

INSERT INTO `cost_categories` (`cost_category_id`, `cost_category`) VALUES
(1, 'Communication and advocacy'),
(2, 'Conferences / workshops'),
(3, 'Direct budget support'),
(4, 'Drugs and food supplies'),
(5, 'Equipment: medical'),
(6, 'Equipment: non-medical'),
(7, 'Overhead/ general administrative costs'),
(8, 'Procurement and supply management'),
(9, 'Program buildings/ infrastructure/ renovation'),
(10, 'Research/ M&E'),
(11, 'Salaries and benefits: government personnel'),
(12, 'Salaries and benefits: non-government personnel'),
(13, 'Technical assistance: external consultants'),
(14, 'Technical assistance: in-country consultants'),
(15, 'Training'),
(16, 'Travel costs: Domestic'),
(17, 'Travel costs: International'),
(18, 'Money not spent'),
(19, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(30) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0 - off, 1 - on',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country`, `status`) VALUES
(1, 'Uganda', 1),
(2, 'Kenya', 0),
(3, 'Tanzania', 0),
(4, 'Rwanda', 0),
(5, 'Burundi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries_populations`
--

CREATE TABLE IF NOT EXISTS `countries_populations` (
  `country_population_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` tinyint(1) unsigned NOT NULL,
  `country_population` int(10) unsigned NOT NULL,
  `country_population_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0 - off, 1 - on',
  `country_population_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`country_population_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `countries_populations`
--

INSERT INTO `countries_populations` (`country_population_id`, `country_id`, `country_population`, `country_population_status`, `country_population_date`) VALUES
(1, 1, 34856813, 1, '2015-04-08 21:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `currency_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `currency_abbreviation` varchar(3) NOT NULL,
  `currency_full` varchar(50) NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`currency_id`, `currency_abbreviation`, `currency_full`) VALUES
(1, 'USD', 'US Dollar'),
(2, 'TZS', 'Tanzanian Shilling'),
(3, 'GBP', 'British Pound'),
(4, 'CNY', 'Chinese Yuan'),
(5, 'EUR', 'Euro'),
(6, 'INR', 'Indian Rupee'),
(7, 'AUD', 'Australian Dollar'),
(8, 'BIF', 'Burundi Franc'),
(9, 'CAD', 'Canadian Dollar '),
(10, 'CZK', 'Czech Koruna'),
(11, 'DKK', 'Danish Krone'),
(12, 'ETB', 'Ethiopian Birr'),
(13, 'FIM', 'Finish Markka'),
(14, 'XAU', 'Gold (oz.)'),
(15, 'HKD', 'Hong Kong Dollar'),
(16, 'ISK', 'Iceland Krona'),
(17, 'ILS', 'Israel New Shekel'),
(18, 'JPY', 'Japanese Yen'),
(19, 'KES', 'Kenyan Shilling'),
(20, 'LUF', 'Luxembourg Franc'),
(21, 'NZD', 'New Zealand Dollar'),
(22, 'NOK', 'Norwegian Krone'),
(23, 'RUB', 'Russian Rouble'),
(24, 'RWF', 'Rwandan Franc'),
(25, 'XAG', 'Silver (oz.)'),
(26, 'SGD', 'Singapore Dollar'),
(27, 'ZAR', 'South African Rand'),
(28, 'KRW', 'South-Korean Won'),
(29, 'SZL', 'Swaziland Lilanqeni'),
(30, 'SEK', 'Swedish Krona'),
(31, 'CHF', 'Swiss Franc'),
(32, 'TWD', 'Taiwan Dollar'),
(33, 'UGX', 'Ugandan Shilling');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `district_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` tinyint(3) unsigned NOT NULL,
  `district` varchar(30) NOT NULL,
  PRIMARY KEY (`district_id`),
  KEY `region_id` (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=118 ;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`district_id`, `region_id`, `district`) VALUES
(1, 1, 'All Districts'),
(2, 2, 'All districts'),
(3, 2, 'Buikwe'),
(4, 2, 'Bukomansimbi'),
(5, 2, 'Butambala'),
(6, 2, 'Buvuma'),
(7, 2, 'Gomba'),
(8, 2, 'Kalangala'),
(9, 2, 'Kalungu'),
(10, 2, 'Kampala'),
(11, 2, 'Kayunga'),
(12, 2, 'Kiboga'),
(13, 2, 'Kyankwanzi'),
(14, 2, 'Luweero'),
(15, 2, 'Lwengo'),
(16, 2, 'Lyantonde'),
(17, 2, 'Masaka'),
(18, 2, 'Mityana'),
(19, 2, 'Mpigi'),
(20, 2, 'Mubende'),
(21, 2, 'Mukono'),
(22, 2, 'Nakaseke'),
(23, 2, 'Nakasongola'),
(24, 2, 'Rakai'),
(25, 2, 'Ssembabule'),
(26, 2, 'Wakiso'),
(27, 3, 'All districts'),
(28, 3, 'Amuria'),
(29, 3, 'Budaka'),
(30, 3, 'Bududa'),
(31, 3, 'Bugiri'),
(32, 3, 'Bukedea'),
(33, 3, 'Bukwo'),
(34, 3, 'Bulambuli'),
(35, 3, 'Busia'),
(36, 3, 'Butaleja'),
(37, 3, 'Buyende'),
(38, 3, 'Iganga'),
(39, 3, 'Jinja'),
(40, 3, 'Kaberamaido'),
(41, 3, 'Kaliro'),
(42, 3, 'Kamuli'),
(43, 3, 'Kapchorwa'),
(44, 3, 'Katakwi'),
(45, 3, 'Kibuku'),
(46, 3, 'Kumi'),
(47, 3, 'Kween'),
(48, 3, 'Luuka'),
(49, 3, 'Manafwa'),
(50, 3, 'Mayuge'),
(51, 3, 'Mbale'),
(52, 3, 'Namayingo'),
(53, 3, 'Namutumba'),
(54, 3, 'Ngora'),
(55, 3, 'Pallisa'),
(56, 3, 'Serere'),
(57, 3, 'Sironko'),
(58, 3, 'Soroti'),
(59, 3, 'Tororo'),
(60, 4, 'All districts'),
(61, 4, 'Buhweju'),
(62, 4, 'Buliisa'),
(63, 4, 'Bundibugyo'),
(64, 4, 'Bushenyi'),
(65, 4, 'Hoima'),
(66, 4, 'Ibanda'),
(67, 4, 'Isingiro'),
(68, 4, 'Kabale'),
(69, 4, 'Kabarole'),
(70, 4, 'Kamwenge'),
(71, 4, 'Kanungu'),
(72, 4, 'Kasese'),
(73, 4, 'Kibaale'),
(74, 4, 'Kiruhura'),
(75, 4, 'Kiryandongo'),
(76, 4, 'Kisoro'),
(77, 4, 'Kyegegwa'),
(78, 4, 'Kyenjojo'),
(79, 4, 'Masindi'),
(80, 4, 'Mbarara'),
(81, 4, 'Mitooma'),
(82, 4, 'Ntoroko'),
(83, 4, 'Ntungamo'),
(84, 4, 'Rubirizi'),
(85, 4, 'Rukungiri'),
(86, 4, 'Sheema'),
(87, 5, 'All districts'),
(88, 5, 'Abim'),
(89, 5, 'Adjumani'),
(90, 5, 'Agago'),
(91, 5, 'Alebtong'),
(92, 5, 'Amolatar'),
(93, 5, 'Amudat'),
(94, 5, 'Amuru'),
(95, 5, 'Apac'),
(96, 5, 'Arua'),
(97, 5, 'Dokolo'),
(98, 5, 'Gulu'),
(99, 5, 'Kaabong'),
(100, 5, 'Kitgum'),
(101, 5, 'Koboko'),
(102, 5, 'Kole'),
(103, 5, 'Kotido'),
(104, 5, 'Lamwo'),
(105, 5, 'Lira'),
(106, 5, 'Maracha'),
(107, 5, 'Moroto'),
(108, 5, 'Moyo'),
(109, 5, 'Nakapiripirit'),
(110, 5, 'Napak'),
(111, 5, 'Nebbi'),
(112, 5, 'Nwoya'),
(113, 5, 'Otuke'),
(114, 5, 'Oyam'),
(115, 5, 'Pader'),
(116, 5, 'Yumbe'),
(117, 5, 'Zombo');

-- --------------------------------------------------------

--
-- Table structure for table `financing_sources`
--

CREATE TABLE IF NOT EXISTS `financing_sources` (
  `financing_source_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `financing_source` varchar(255) NOT NULL,
  `added_by` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0 - added by system, and any number greater than zero means the ID of the account that added the record',
  `financing_source_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - not approved, 1 - approved, 2 - discarded',
  PRIMARY KEY (`financing_source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `financing_sources`
--

INSERT INTO `financing_sources` (`financing_source_id`, `financing_source`, `added_by`, `financing_source_status`) VALUES
(1, 'Civil Soiety Fund', 2, 2),
(2, 'WHO', 2, 1),
(3, 'USAID', 2, 1),
(4, 'UNFPA', 2, 1),
(5, 'Civil Society Fund', 2, 1),
(6, 'Elma Philanthropies', 2, 1),
(7, 'Arc UK', 2, 1),
(8, 'DFID-UK', 2, 1),
(9, 'UNICEF', 2, 1),
(10, 'Global Fund', 2, 1),
(11, 'World Bank', 2, 1),
(12, 'GAVI', 2, 1),
(13, 'Government of Uganda', 2, 1),
(14, 'SIDA', 2, 1),
(15, 'UNEPI', 2, 1),
(16, 'Cofunding (Elma Philanthropies and Arc UK)', 5, 1),
(17, 'Research for Development (R4D)', 6, 2),
(18, 'Results for Development (R4D)', 2, 1),
(19, 'world vision', 8, 2),
(20, 'world vision', 8, 1),
(21, 'world vision', 8, 2),
(22, 'CDC', 2, 1),
(23, 'AMICALL', 2, 1),
(24, 'IRCS', 2, 1),
(25, 'CORDAID', 2, 1),
(26, 'Population Council', 2, 1),
(27, 'IDA', 2, 1),
(28, 'Government of Tuscany', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notification_type` tinyint(2) unsigned NOT NULL COMMENT '1 - new organization needs approval, 2 - New financing agent needs approval',
  `notification_trigger_id` int(10) unsigned NOT NULL COMMENT 'The item id that triggered the notification',
  `notification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notification_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - Not attended to, 1 - attended to',
  PRIMARY KEY (`notification_id`),
  UNIQUE KEY `notification_type` (`notification_type`,`notification_trigger_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `notification_type`, `notification_trigger_id`, `notification_date`, `notification_status`) VALUES
(1, 2, 1, '2015-04-09 07:07:05', 1),
(2, 2, 2, '2015-04-09 07:09:20', 1),
(3, 2, 3, '2015-04-09 07:09:26', 1),
(4, 2, 4, '2015-04-09 07:09:35', 1),
(5, 2, 5, '2015-04-09 07:10:53', 1),
(6, 2, 6, '2015-04-09 07:16:15', 1),
(7, 2, 7, '2015-04-09 07:21:09', 1),
(8, 2, 8, '2015-04-09 07:23:50', 1),
(9, 2, 9, '2015-04-09 07:24:04', 1),
(10, 2, 10, '2015-04-09 07:26:18', 1),
(11, 2, 11, '2015-04-09 07:28:43', 1),
(12, 2, 12, '2015-04-09 07:28:57', 1),
(13, 2, 13, '2015-04-09 07:29:33', 1),
(14, 2, 14, '2015-04-09 07:30:08', 1),
(15, 2, 15, '2015-04-09 07:51:08', 1),
(16, 1, 1, '2015-04-09 09:06:24', 1),
(17, 1, 2, '2015-04-09 11:53:33', 1),
(18, 1, 3, '2015-04-09 13:08:03', 1),
(19, 2, 16, '2015-04-10 13:59:24', 1),
(20, 1, 4, '2015-04-13 13:45:37', 1),
(21, 2, 17, '2015-04-13 13:49:55', 1),
(22, 2, 18, '2015-04-13 13:51:23', 1),
(23, 1, 5, '2015-04-15 12:02:40', 1),
(24, 1, 6, '2015-04-15 12:03:18', 1),
(25, 2, 19, '2015-04-15 12:29:03', 1),
(26, 2, 20, '2015-04-15 12:33:51', 1),
(27, 2, 21, '2015-04-15 12:35:34', 1),
(28, 2, 22, '2015-04-15 13:31:35', 1),
(29, 2, 23, '2015-04-15 13:31:56', 1),
(30, 2, 24, '2015-04-15 13:34:08', 1),
(31, 2, 25, '2015-04-15 13:42:42', 1),
(32, 2, 26, '2015-04-15 13:43:34', 1),
(33, 2, 27, '2015-04-15 14:15:06', 1),
(34, 1, 7, '2015-04-17 10:44:41', 1),
(35, 2, 28, '2015-04-17 10:47:09', 1),
(36, 1, 8, '2015-04-21 12:28:15', 1),
(37, 1, 9, '2015-04-22 09:43:31', 1),
(38, 1, 10, '2015-04-29 08:25:53', 1),
(39, 1, 11, '2015-04-29 12:32:38', 1),
(40, 1, 12, '2015-04-30 09:26:42', 1),
(41, 1, 13, '2015-05-08 06:38:45', 1),
(42, 1, 14, '2015-05-08 06:42:16', 1),
(43, 1, 15, '2015-05-08 07:23:26', 1),
(44, 1, 16, '2015-06-29 09:22:58', 1),
(45, 1, 17, '2015-07-24 07:44:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE IF NOT EXISTS `organizations` (
  `organization_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` mediumint(8) unsigned NOT NULL,
  `organization_name` varchar(255) NOT NULL,
  `organization_type_id` tinyint(1) unsigned NOT NULL,
  `fiscal_year_start_month` tinyint(2) unsigned NOT NULL COMMENT '1-12 representing month',
  `pooled_funds` tinyint(1) unsigned NOT NULL COMMENT '1 - Yes, 0 - No.  	 Is your organization currently providing or have they in the past provided any pooled funds for health from 2012-2016',
  `mou_with_moh` tinyint(1) unsigned NOT NULL COMMENT '1 - Yes, 0 - No. Did you sign a MOU with the Ministry of Health?',
  `district_work_start` year(4) NOT NULL DEFAULT '0000',
  `authority_id` mediumint(8) unsigned NOT NULL,
  `other_authority` varchar(255) NOT NULL,
  `contact_name` varchar(25) NOT NULL COMMENT 'Point of contact name',
  `mobile_phone_number` varchar(255) NOT NULL COMMENT 'Point of contact mobile number',
  `office_phone_number` varchar(255) NOT NULL COMMENT 'Point of contact office number',
  `email_address` varchar(100) NOT NULL COMMENT 'Point of contact email address',
  `data_source` varchar(255) NOT NULL COMMENT 'The source of data and assumptions used to complete the entry',
  PRIMARY KEY (`organization_id`),
  KEY `organization_type_id` (`organization_type_id`),
  KEY `account_id` (`account_id`),
  KEY `authority_id` (`authority_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`organization_id`, `account_id`, `organization_name`, `organization_type_id`, `fiscal_year_start_month`, `pooled_funds`, `mou_with_moh`, `district_work_start`, `authority_id`, `other_authority`, `contact_name`, `mobile_phone_number`, `office_phone_number`, `email_address`, `data_source`) VALUES
(1, 3, 'National Medical Stores', 5, 6, 0, 1, 0000, 1, '', 'Samasha', '+256754082919', '+256414660792', 'casiimwe@samasha.org', 'NMS was established as a Statutory Corporation in 1993. It is mandated to Procure, Stores & Distribute Essential Medicines and Medical Supplies to all Public Health Facilities in the Country, inclusive of police, army and prisons. In addition, NMS distrib'),
(2, 4, 'UNICEF', 4, 7, 1, 1, 1990, 1, '', 'Neelam Bhardwaj', '+256717171402', '+256417171402', 'nbhardwaj@unicef.org', 'Keeping child alive and thriving, safe and learning.'),
(3, 5, 'CLINTON HEALTH ACCESS INITIATIVE', 1, 1, 0, 1, 0000, 1, '', 'Damien Kirchoffer', '+2567779500658; +256777800098', '+256772673300', 'lkabunga@clintonhealthacccess.org', 'CHAI-Uganda is part of the CHAI global health organization committed to strengthening integrated health systems in the developing world and expanding access to care and treatment. The CHAI Uganda office works across multiple disease areas and Ministry of '),
(4, 6, 'HEPS Uganda', 2, 1, 0, 1, 2010, 3, '', 'Carol Aruho', '+256701140361', '+256414270970', 'caruho@heps.or.ug', 'Established in November 2000, the Coalition for Health promotion and Social Development (HEPS– Uganda) is a health and human rights organization that advocates   for health rights and health responsibilities of Ugandans especially giving voice to the poor'),
(5, 7, 'World Bank', 4, 1, 1, 1, 0000, 1, '', 'Samasha', '+256754082919', '+256414660792', 'casiimwe@samasha.org', 'World Bank supports the Health System Strengthening Project under the Ministry of Health'),
(6, 8, 'uganda prisons', 5, 7, 1, 1, 2008, 1, '', 'Dr, kisambu JAMES', '+25677292716/4', '+256718495571', 'drkisambu@gmail.com', 'Uganda prisons is a security organisation whose mission is to keep custody of offenders andd reintergrate them back to the comunity as responsible citzens'),
(7, 9, 'Doctors with Africa CUAMM', 1, 1, 1, 0, 2007, 1, '', 'Dr. Peter Lochoro', '+256752853501', '+256414267858', 'p.lochoro@cuamm.org', 'An Health NGO whose main scope of work is health systems strengthening, maternal and child health, nutrition. Main operation areas Karamoja districts and Oyam. Italian International NGO'),
(8, 10, 'Jhpiego Corporation', 1, 6, 1, 0, 2012, 1, '', 'Emily Katarikawe', '+256759244700', '+256312202937', 'emily.katarikawe@jhpiego.org', 'Jhpiego is an international, non-profit health organization affiliated with Johns Hopkins University. For 40 years, and in over 155 countries, Jhpiego has worked to prevent the needless deaths of women and their families. In Uganda, Jhpiego is working to '),
(9, 11, 'PATH', 1, 1, 1, 1, 0000, 1, '', 'Samasha', '+256754082919', '+256414660792', 'casiimwe@samasha.org', 'PATH is the leader in global health innovation. An international nonprofit organization, we save lives and improve health, especially among women and children. \r\nPATH is driven by an unshakeable commitment to health equity and a bold belief in the power o'),
(10, 12, 'SIDA Uganda', 3, 1, 1, 1, 0000, 1, '', 'Samasha Medical Foundatio', '+256754082919', '+256414660792', 'casiimwe@samasha.org', 'SIDA works on behalf of the Swedish parliament and Government with a mission of reducing poverty in the world and are committed to enabling people live a better life'),
(11, 13, 'Ministry of Health', 5, 6, 1, 0, 0000, 2, 'Parliament', 'Tom Aliti', '+256772574789', '+256414231584', 'casiimwe@samasha.org', 'Ministry of Health is a Government Ministry responsible for one of the important sectors that takes care of promoting a healthy and productive life for the population in Uganda.\r\n\r\nThis Ministry aims to have the highest possible level of health services t'),
(12, 14, 'SAMASHA MEDICAL FOUNDATION', 2, 1, 0, 1, 0000, 1, '', 'Dr. Moses Muwonge', '+256772537722', '+256414660792', 'mmuwonge@samasha.org', 'Samasha Medical Foundation is a Non-Governmental Organization registered in Uganda by the NGO Board (Reg. No. 10613). Samasha advocates for improved health systems in East Africa, specifically through health commodity security and financing, health servic'),
(13, 15, 'East African Community Secretariat', 3, 7, 1, 1, 1990, 1, '', 'Mike Mukirane', '+256772462125', '+256701462125', 'mmukirane@eachq.org', 'Intergovernmental Organisation'),
(14, 16, 'Health Child', 2, 10, 0, 1, 2006, 3, '', 'Gabala Franco', '+256779585340', '+256 0473660074', 'balidawafrank@healthchild.org.ug', 'Introduction \r\nHealth Child started way back in 2006 as a CBO in Jinja district and over time, the organization has evolved and broadened its scope of focus. Currently, Health Child is a fully registered Non-Government Organization in Uganda (Reg no: 9541'),
(15, 17, 'United Nations Population Fund', 3, 1, 1, 1, 0000, 1, '', 'Immaculate Nalikka', '+256752381756', '+256417744500', 'nalikka@unfpa.org', 'UNFPA is the lead UN agency for delivering a world where every pregnancy is wanted, every birth is safe, and every young person''s potential is fulfilled'),
(16, 18, 'Makerere University', 5, 7, 1, 1, 2010, 1, '', 'Dr. Justine Bukenya', '+256772446355', '+256414222725', 'jbukenya@musph.ac.ug', 'Academic institutions interested in implementation research'),
(17, 19, 'Uganda Health Supply Chain Program', 4, 10, 0, 0, 2014, 1, '', 'Eric Jemera Nabuguzi', '+256772938356', '+256414235038', 'jemera@gmail.com', 'The USAID-funded program, Uganda Health Supply Chain (UHSC), aims to assist the Government of Uganda’s and the Ministry of Health’s commitment to improve the health status of the Ugandan population by increasing the availability, affordability, accessibil');

-- --------------------------------------------------------

--
-- Table structure for table `organization_types`
--

CREATE TABLE IF NOT EXISTS `organization_types` (
  `organization_type_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `organization_type` varchar(25) NOT NULL,
  PRIMARY KEY (`organization_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `organization_types`
--

INSERT INTO `organization_types` (`organization_type_id`, `organization_type`) VALUES
(1, 'International NGO'),
(2, 'Local NGO'),
(3, 'Bilateral'),
(4, 'Multi-Lateral'),
(5, 'GOU'),
(6, 'Private');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `project_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organization_id` mediumint(8) unsigned NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_description` text NOT NULL,
  `financing_source_id` tinyint(3) unsigned NOT NULL,
  `financing_agent` mediumint(8) unsigned NOT NULL COMMENT 'Organization ID, got from the organizations table',
  `implementing_agent` mediumint(8) unsigned NOT NULL COMMENT 'Organization ID, got from the organizations table',
  `project_entry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0 - off, 1 - on',
  PRIMARY KEY (`project_id`),
  KEY `organization_id` (`organization_id`),
  KEY `financing_source` (`financing_source_id`,`financing_agent`,`implementing_agent`),
  KEY `financing_agent` (`financing_agent`),
  KEY `implementing_agent` (`implementing_agent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `organization_id`, `project_name`, `project_description`, `financing_source_id`, `financing_agent`, `implementing_agent`, `project_entry_date`, `project_status`) VALUES
(1, 1, 'Procurement, Storage and Distribution of Essential Medicines', 'National Medical Stores is Mandated to Procure, Stores & Distribute Essential Medicines and Medical Supplies to all Public Health Facilities in the Country including the police, army and prisons. Effective 2012, NMS took on distribution of Vaccines across the country.\r\n\r\nNMS has enough stock of Medicines and distributes to all Government hospitals and health centers on a routine basis against the orders made to us by health facilities. NMS also has "Last Mile delivery system for medicines and other medical supplies to ensure that medical supplies reach the end user at all government health facilities across the country safe and on time.', 13, 1, 1, '2015-04-09 09:43:50', 1),
(2, 2, 'strengthening the continum of care for MNH', 'bbnjjkjj', 14, 2, 2, '2015-04-09 12:06:26', 1),
(3, 6, 'HIV', 'PMTCT SUPPORT', 10, 6, 6, '2015-04-15 12:37:50', 1),
(4, 4, 'Resoure Allocation and access to Essential Medicines in Uganda', 'Community Score Card (CSC) project which was implemented at 5 health centres (2 HC IIs, 2 HC III, and 1 HC IV) in Lira to address access to medicines issues.', 18, 4, 4, '2015-04-15 12:58:50', 1),
(5, 4, 'Assessing Coverage and availability of ORS and Zinc for the treatment of diarrhoea in Uganda', 'Assessing the uptake and community knowledge on use of ORS and Zinc by users and providers in various districts of Uganda from all the key regions, (Nothern, Eastern, Western and Central).', 16, 3, 4, '2015-04-15 13:09:38', 1),
(6, 9, 'Uganda Advocacy for Better Health', 'This project aims at achieving  three objectives: (1) increased citizen participation in planning and monitoring of health and social services; (2) strengthening the capacity of CSOs to conduct advocacy in target districts; and (3) strengthening the institutional capacity of CSOs. ABH in partnership with civil society organizations (CSOs) operating across multiple levels will catalyze advocacy action among community members and propel change in health and social service delivery in 35 districts in southern Uganda.', 3, 9, 4, '2015-04-22 09:52:51', 1),
(7, 10, 'Output-based Aid Coupons for Maternal Health', 'This project is aimed at subsidizing the cost o to pregnant women and new mothers to quality health care', 14, 10, 5, '2015-04-29 08:35:23', 1),
(8, 10, 'Maternal and Newborn Care', 'Support to maternal and child health in five districts in Karamoja  by enhancing continuum of care for maternal and new born health. With the introduction of a comprehensive MNCH Package, innovations and social mobilisation were used to mobilise mothers for maternal and new-born health services in the Karamoja region.', 14, 10, 2, '2015-04-29 08:53:23', 1),
(9, 5, 'Uganda Health System Streghtening Project', 'The main objective of this project is to deliver the Uganda National Minimum Health Care Package (UNMHCP) to Ugandans, with a focus on maternal health, newborn care, and family planning. The additional credit will cover the funding shortfall for the renovation of the health facilities selected under the original project. It will ensure basic functionality of the facilities and scale up services for maternal care.', 27, 5, 11, '2015-04-29 12:38:14', 1),
(10, 11, 'GAVI Immunisation Support System (ISS 1)', 'GAVI ISS 1', 12, 11, 11, '2015-04-30 06:18:18', 1),
(11, 11, 'GAVI Immunisation Support System (ISS 2)', 'GAVI ISS 2', 12, 11, 11, '2015-04-30 06:19:02', 1),
(12, 11, 'GAVI HSS Health systems strengthening', 'GAVI HSS Health systems strengthening', 12, 11, 11, '2015-04-30 06:19:34', 1),
(13, 11, 'GAVI PVC', 'GAVI PVC', 12, 11, 11, '2015-04-30 06:23:09', 1),
(14, 9, 'Uganda Advocacy for Better Health', 'his project aims at achieving three objectives: (1) increased citizen participation in planning and monitoring of health and social services; (2) strengthening the capacity of CSOs to conduct advocacy in target districts; and (3) strengthening the institutional capacity of CSOs. ABH in partnership with civil society organizations (CSOs) operating across multiple levels will catalyze advocacy action among community members and propel change in health and social service delivery in 35 districts in southern Uganda.', 3, 9, 9, '2015-04-30 08:00:14', 1),
(15, 12, 'PATH UNCoLSC ADVOCACY', 'There is a current unacceptable condition where millions of women and children die from medical conditions that could have been easily prevented and treated through access to existing medicines and other health commodities. The UN Commission on Lifesaving commodities for women and children set out to define a priority list of 13 overlooked lifesaving commodities for women and children through identifying the key barriers preventing access to and use of these commodities. In Uganda, due to identification of an inadequate conducive policy environment to support implementation of the RMNCH catalytic plan to achieve its desired goal of removing bottlenecks to use and access to RMNCH commodities, Samasha Medical Foundation received a grant from PATH to advocate for inclusion of selected RMNCH commodities onto the Essential Medicines List(EML) and for use at HC IIs, IIIs and community level by midwives and nurses.', 9, 9, 12, '2015-04-30 10:09:13', 1),
(16, 4, 'Consolidating National Priorities to Increasing Access to LSCs', 'There is a current unacceptable condition where millions of women and children die from medical conditions that could have been easily prevented and treated through access to existing medicines and other health commodities. The UN Commission on Lifesaving commodities for women and children set out to define a priority list of 13 overlooked lifesaving commodities for women and children through identifying the key barriers preventing access to and use of these commodities.', 4, 9, 4, '2015-04-30 12:39:26', 1),
(17, 4, 'Consolidating National Priorities to Increasing Access to LSCs', 'There is a current unacceptable condition where millions of women and children die from medical conditions that could have been easily prevented and treated through access to existing medicines and other health commodities. The UN Commission on Lifesaving commodities for women and children set out to define a priority list of 13 overlooked lifesaving commodities for women and children through identifying the key barriers preventing access to and use of these commodities.', 9, 9, 4, '2015-04-30 12:43:07', 1),
(18, 8, 'Developing Longterm flexible MNH resource for Karamoja', 'Projects aims at training master trainers for Karamoja region to enhance skills maternal new bron care services with special focus on :\r\n\r\na) Pre- eclampsia and eclampsia\r\nb) Helping mother''s survive bleeding after birth (HMS-BAB)\r\nc) Helping babies breathe(HBB)\r\nd) management of prolonged and obstructed labor\r\nMaster trainers are expected to impart acquired knowledge and skills to other MNH providers at lower level units within karamoja region', 9, 2, 8, '2015-05-04 12:05:33', 1),
(19, 11, 'Training and Capacity Building for Service Providers- Child Health', 'Sick under fives and newborns in targeted accessing timely, appropriate medical care.', 13, 11, 11, '2015-05-07 08:19:21', 1),
(20, 11, 'Technical Support, Monitoring and Evaluation of Service Provides and Facilities', 'Emergency care for newborn and under-fives at second referral care level strengthened', 13, 11, 11, '2015-05-07 08:33:42', 1),
(21, 11, 'Policies, Lawa, Guidelines, Plans and Strategies', 'Coordination, follow up of Child Survival Strategy and Partnerships with other involved sectors', 13, 11, 11, '2015-05-07 10:03:20', 1),
(22, 3, 'Dummy Entry', 'Dummy entry', 16, 3, 3, '2015-05-13 08:53:27', 1),
(23, 12, 'RMNCN Resource Tracking', 'Tracking of Expenditures to RMNCH by Financing Source to Implementing Partners', 4, 11, 12, '2015-06-27 08:13:06', 1),
(24, 16, 'makerere university', 'family and reproductive health', 13, 16, 16, '2015-07-01 13:00:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects_geography`
--

CREATE TABLE IF NOT EXISTS `projects_geography` (
  `project_geography_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `country_id` tinyint(1) unsigned NOT NULL,
  `region_id` tinyint(1) unsigned NOT NULL,
  `district_id` smallint(4) unsigned NOT NULL,
  `district_partners` text NOT NULL,
  `project_geography_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 - on, 0 - off',
  PRIMARY KEY (`project_geography_id`),
  KEY `project_id` (`project_id`,`country_id`,`region_id`,`district_id`),
  KEY `country_id` (`country_id`),
  KEY `region_id` (`region_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `projects_geography`
--

INSERT INTO `projects_geography` (`project_geography_id`, `project_id`, `country_id`, `region_id`, `district_id`, `district_partners`, `project_geography_status`) VALUES
(1, 1, 1, 1, 1, '', 1),
(2, 3, 1, 1, 1, 'DHO and CBOs', 1),
(3, 4, 1, 5, 105, '', 1),
(5, 5, 1, 1, 1, '', 1),
(7, 7, 1, 1, 1, '', 1),
(10, 7, 1, 5, 107, '', 1),
(11, 7, 1, 5, 103, '', 1),
(12, 7, 1, 5, 99, '', 1),
(13, 8, 1, 5, 107, '', 1),
(14, 8, 1, 5, 103, '', 1),
(15, 9, 1, 1, 1, '', 1),
(16, 13, 1, 1, 1, '', 1),
(18, 10, 1, 1, 1, '', 1),
(19, 11, 1, 1, 1, '', 1),
(20, 12, 1, 1, 1, '', 1),
(21, 6, 1, 4, 66, '', 0),
(22, 6, 1, 4, 74, '', 0),
(23, 6, 1, 4, 66, '', 0),
(24, 6, 1, 1, 1, '', 0),
(25, 6, 1, 4, 66, '', 1),
(26, 14, 1, 1, 1, '', 1),
(27, 15, 1, 1, 1, '', 1),
(28, 17, 1, 1, 1, '', 1),
(29, 19, 1, 1, 1, '', 1),
(30, 20, 1, 1, 1, '', 1),
(31, 21, 1, 1, 1, '', 1),
(32, 22, 1, 1, 1, 'NA', 1),
(33, 23, 1, 2, 10, '', 1),
(34, 23, 1, 4, 76, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `region_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` tinyint(3) unsigned NOT NULL,
  `region` varchar(25) NOT NULL,
  PRIMARY KEY (`region_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`region_id`, `country_id`, `region`) VALUES
(1, 1, 'National'),
(2, 1, 'Central'),
(3, 1, 'Eastern'),
(4, 1, 'Western'),
(5, 1, 'Nothern');

-- --------------------------------------------------------

--
-- Table structure for table `support_budgets`
--

CREATE TABLE IF NOT EXISTS `support_budgets` (
  `support_budget_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `currency_id` tinyint(2) unsigned NOT NULL,
  `total_budget` bigint(15) unsigned NOT NULL COMMENT 'Money before converting to local currence',
  `exchange_rate` smallint(5) unsigned NOT NULL DEFAULT '1',
  `local_currency_ammount` bigint(15) NOT NULL COMMENT 'The amount in the local currency of the country where the funds are going to be used',
  `money_not_spent` bigint(15) unsigned NOT NULL COMMENT 'Money not spent in local currency',
  `support_budget_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`support_budget_id`),
  KEY `project_id` (`project_id`,`currency_id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `support_budgets`
--

INSERT INTO `support_budgets` (`support_budget_id`, `project_id`, `currency_id`, `total_budget`, `exchange_rate`, `local_currency_ammount`, `money_not_spent`, `support_budget_date`) VALUES
(1, 1, 33, 219374587000, 1, 219374587000, 0, '2015-04-09 09:48:42'),
(2, 4, 33, 24545400, 1, 24545400, 0, '2015-04-15 13:00:50'),
(3, 5, 33, 439708500, 1, 439708500, 0, '2015-04-15 13:11:22'),
(4, 7, 1, 3838359, 2500, 9595897500, 9595897500, '2015-04-29 08:43:36'),
(5, 8, 1, 767672, 2500, 1919180000, 1919180000, '2015-04-29 09:03:02'),
(6, 9, 1, 28984745, 2500, 72461862500, 72461862500, '2015-04-29 13:00:18'),
(7, 13, 33, 514533052, 1, 514533052, 0, '2015-04-30 06:33:26'),
(8, 10, 33, 169139020, 1, 169139020, 0, '2015-04-30 06:45:02'),
(9, 11, 33, 605206500, 1, 605206500, 0, '2015-04-30 07:22:39'),
(10, 12, 33, 223878800, 1, 223878800, 0, '2015-04-30 07:41:36'),
(11, 6, 33, 427000000, 1, 427000000, 427000000, '2015-04-30 07:53:03'),
(12, 14, 1, 4, 2500, 10000, 10000, '2015-04-30 08:04:30'),
(13, 15, 1, 30000, 2500, 75000000, 0, '2015-04-30 10:17:01'),
(14, 17, 33, 25059840, 1, 25059840, 0, '2015-04-30 12:46:04'),
(15, 19, 33, 43187000, 1, 43187000, 0, '2015-05-07 08:24:39'),
(16, 20, 33, 117893000, 1, 117893000, 0, '2015-05-07 09:40:08'),
(17, 21, 33, 111536000, 1, 111536000, 0, '2015-05-07 10:15:08'),
(18, 22, 1, 10000, 2500, 25000000, 25000000, '2015-05-13 09:47:28');

-- --------------------------------------------------------

--
-- Table structure for table `support_budget_expenses`
--

CREATE TABLE IF NOT EXISTS `support_budget_expenses` (
  `support_budget_expense_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `cost_category_id` tinyint(2) unsigned NOT NULL,
  `cost` bigint(15) unsigned NOT NULL COMMENT 'Amount of money spent from the total budget on a particular cost category',
  `other_cost_category_name` varchar(255) NOT NULL COMMENT 'This is the name of the cost in case one choses ''other as the cost category''',
  `not_spent_reason` text NOT NULL,
  `support_budget_expense_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `support_budget_expense_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0 - off, 1 - on',
  PRIMARY KEY (`support_budget_expense_id`),
  KEY `project_id` (`project_id`,`cost_category_id`),
  KEY `cost_category_id` (`cost_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `support_budget_expenses`
--

INSERT INTO `support_budget_expenses` (`support_budget_expense_id`, `project_id`, `cost_category_id`, `cost`, `other_cost_category_name`, `not_spent_reason`, `support_budget_expense_date`, `support_budget_expense_status`) VALUES
(1, 1, 7, 43, '', '', '2015-04-14 13:54:47', 0),
(2, 1, 7, 43874917400, '', '', '2015-04-14 13:55:26', 1),
(3, 1, 8, 175499669600, '', '', '2015-04-14 14:01:32', 1),
(4, 4, 1, 24545400, '', '', '2015-04-15 13:02:20', 1),
(5, 5, 12, 57501500, '', '', '2015-04-15 13:11:39', 1),
(6, 5, 15, 48862300, '', '', '2015-04-15 13:12:00', 1),
(7, 5, 10, 333344700, '', '', '2015-04-15 13:12:19', 1),
(8, 13, 15, 62885000, '', '', '2015-04-30 06:36:34', 1),
(9, 13, 2, 12049000, '', '', '2015-04-30 06:37:13', 1),
(10, 13, 19, 439599052, 'Sub-county Sensitization', '', '2015-04-30 06:38:00', 1),
(11, 10, 19, 34841520, 'Capacity Building', '', '2015-04-30 06:45:42', 1),
(12, 10, 8, 6719500, '', '', '2015-04-30 06:51:37', 1),
(13, 10, 10, 37220000, '', '', '2015-04-30 07:18:50', 1),
(14, 10, 19, 90358000, 'Strengthening Immunisation Services', '', '2015-04-30 07:20:21', 1),
(15, 11, 19, 88048000, 'Mobilization', '', '2015-04-30 07:24:00', 1),
(16, 11, 19, 72886000, 'Integrated Service delivery', '', '2015-04-30 07:24:50', 1),
(17, 11, 19, 444272500, 'District Support', '', '2015-04-30 07:29:17', 1),
(18, 12, 19, 223878800, 'Capacity Building', '', '2015-04-30 07:42:12', 1),
(19, 15, 12, 42100000, '', '', '2015-04-30 10:22:05', 1),
(20, 15, 14, 7500000, '', '', '2015-04-30 10:22:31', 1),
(21, 15, 16, 6550000, '', '', '2015-04-30 10:23:00', 0),
(22, 15, 16, 9398575, '', '', '2015-04-30 10:36:18', 1),
(23, 15, 19, 4676125, 'Communication (Telephone and Internet)', '', '2015-04-30 10:37:35', 1),
(24, 15, 19, 8967500, 'Stationery', '', '2015-04-30 10:38:35', 1),
(25, 15, 2, 2357800, '', '', '2015-04-30 10:39:49', 1),
(26, 17, 12, 5401838, '', '', '2015-04-30 12:46:34', 1),
(27, 17, 7, 3480000, '', '', '2015-04-30 12:47:22', 1),
(28, 17, 19, 11000000, 'Consultancy', '', '2015-04-30 12:48:11', 1),
(29, 17, 10, 5178002, '', '', '2015-04-30 12:49:07', 1),
(30, 19, 14, 2945000, '', '', '2015-05-07 08:25:16', 1),
(31, 19, 16, 10380000, '', '', '2015-05-07 08:25:34', 1),
(32, 19, 2, 3084000, '', '', '2015-05-07 08:26:22', 1),
(33, 19, 19, 5341000, 'Allowances', '', '2015-05-07 08:29:53', 1),
(34, 19, 7, 8103000, '', '', '2015-05-07 08:30:28', 1),
(35, 19, 19, 10580000, 'General Supply of Goods and Services', '', '2015-05-07 08:31:08', 1),
(36, 19, 19, 2754000, 'Hire of Venue', '', '2015-05-07 08:31:54', 1),
(37, 20, 16, 50000000, '', '', '2015-05-07 09:43:44', 1),
(38, 20, 2, 4091000, '', '', '2015-05-07 09:45:53', 1),
(39, 20, 19, 28070000, 'General Supply of Goods and Services', '', '2015-05-07 09:47:56', 1),
(40, 20, 7, 10171000, '', '', '2015-05-07 09:48:54', 1),
(41, 20, 19, 21965000, 'Maintainance of Equipment', '', '2015-05-07 09:52:18', 1),
(42, 20, 14, 2220000, 'Hire of Venue', '', '2015-05-07 09:53:13', 1),
(43, 20, 19, 1376000, 'Hire of Venue', '', '2015-05-07 09:53:54', 1),
(44, 21, 16, 53240000, '', '', '2015-05-07 10:16:15', 1),
(45, 21, 19, 14170000, 'Allowances', '', '2015-05-07 10:17:06', 1),
(46, 21, 14, 2945000, '', '', '2015-05-07 10:17:27', 1),
(47, 21, 19, 10776000, 'Maintainance of Equipment', '', '2015-05-07 10:18:38', 1),
(48, 21, 7, 19587000, '', '', '2015-05-07 10:20:31', 1),
(49, 21, 19, 6727000, 'Hire of Venue', '', '2015-05-07 10:23:02', 1),
(50, 21, 2, 4091000, '', '', '2015-05-07 10:24:11', 1),
(51, 22, 3, 8000, '', '', '2015-05-13 10:06:57', 0),
(52, 22, 14, 2000, '', '', '2015-05-13 10:07:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `support_sub_types`
--

CREATE TABLE IF NOT EXISTS `support_sub_types` (
  `support_sub_type_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `support_type_id` tinyint(1) unsigned NOT NULL,
  `support_sub_type` varchar(100) NOT NULL,
  `system_added` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - No, 1 - Yes. Was it system added or added by the user',
  PRIMARY KEY (`support_sub_type_id`),
  KEY `support_type_id` (`support_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `support_sub_types`
--

INSERT INTO `support_sub_types` (`support_sub_type_id`, `support_type_id`, `support_sub_type`, `system_added`) VALUES
(1, 1, 'Gynecology', 1),
(2, 1, 'Adolescent reproductive health', 1),
(3, 1, 'Reproductive cancers', 1),
(4, 1, 'Fistula repair', 1),
(5, 1, 'Gender-based violence', 1),
(6, 1, 'STI prevention and management', 1),
(7, 2, 'Cross cutting family planning activities', 1),
(8, 2, 'Oral contraceptive pill', 1),
(9, 2, 'Depo-provera injection', 1),
(10, 2, 'Bilateral tubular ligation/vasectomy', 1),
(11, 2, 'Emergency contraception', 1),
(12, 2, 'Male condoms', 1),
(13, 2, 'Female condoms', 1),
(14, 2, 'Norplant/implant', 1),
(15, 2, 'IUD', 1),
(16, 3, 'Non operative facility delivery', 1),
(17, 3, 'Operative deliveries', 1),
(18, 3, 'Antenatal care', 1),
(19, 3, 'Post natal care', 1),
(20, 3, 'EMONC', 1),
(21, 3, ' Post abortion care', 1),
(22, 3, 'Essential newborn care', 1),
(23, 3, 'Civil registration and death audits', 1),
(24, 3, 'PMTCT', 1),
(25, 3, 'Malaria treatment and prevention', 1),
(26, 4, 'General quality pediatric care', 1),
(27, 4, 'Diarrheal diseases', 1),
(28, 4, 'Case management of pneumonia', 1),
(29, 4, 'Integrated care management', 1),
(30, 4, 'Civil registration and death audits', 1),
(31, 4, 'PMTCT', 1),
(32, 4, 'Malaria treatment and prevention', 1),
(33, 5, 'Full immunization', 1),
(34, 5, 'Diptheria, tetanus, pertussis, haemophilus influenae, hepatitis B', 1),
(35, 5, 'Oral polio vaccine (OPV)', 1),
(36, 5, 'Tetanus toxoid', 1),
(37, 5, 'Pneumococcal conjugate vaccine (PCV)', 1),
(38, 5, 'Rotavirus vaccine', 1),
(39, 5, 'Cold chain', 1),
(40, 5, 'Measles', 1),
(41, 6, 'General nutritional food support', 1),
(42, 6, 'Management of acute malnutrition', 1),
(43, 6, 'Vitamin A', 1),
(44, 6, 'Growth monitoring and promotion', 1),
(45, 6, 'Breastfeeding support', 1),
(46, 6, 'Complementary feeding', 1),
(47, 6, 'Micronutrient supplementation', 1),
(48, 6, 'Iron/folic acid supplementation', 1),
(49, 6, 'Hemoglobin level', 1),
(50, 7, 'All activities', 1);

-- --------------------------------------------------------

--
-- Table structure for table `support_types`
--

CREATE TABLE IF NOT EXISTS `support_types` (
  `support_type_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `support_type` varchar(30) NOT NULL,
  PRIMARY KEY (`support_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `support_types`
--

INSERT INTO `support_types` (`support_type_id`, `support_type`) VALUES
(1, 'Reproductive Health'),
(2, 'Family Planning'),
(3, 'Maternal and Neonatal Health'),
(4, 'Child Health'),
(5, 'Vaccines'),
(6, 'Nutrition'),
(7, 'Cross cutting RMNCH Activities'),
(8, 'Other');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `activities_ibfk_2` FOREIGN KEY (`support_type_id`) REFERENCES `support_types` (`support_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `activities_ibfk_3` FOREIGN KEY (`support_sub_type_id`) REFERENCES `support_sub_types` (`support_sub_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `regions` (`region_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `organizations_ibfk_3` FOREIGN KEY (`organization_type_id`) REFERENCES `organization_types` (`organization_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `organizations_ibfk_4` FOREIGN KEY (`authority_id`) REFERENCES `authorities` (`authority_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `projects_ibfk_3` FOREIGN KEY (`financing_agent`) REFERENCES `organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `projects_ibfk_4` FOREIGN KEY (`implementing_agent`) REFERENCES `organizations` (`organization_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `projects_ibfk_5` FOREIGN KEY (`financing_source_id`) REFERENCES `financing_sources` (`financing_source_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `projects_geography`
--
ALTER TABLE `projects_geography`
  ADD CONSTRAINT `projects_geography_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `projects_geography_ibfk_2` FOREIGN KEY (`region_id`) REFERENCES `regions` (`region_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `projects_geography_ibfk_3` FOREIGN KEY (`district_id`) REFERENCES `districts` (`district_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `projects_geography_ibfk_4` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `regions`
--
ALTER TABLE `regions`
  ADD CONSTRAINT `regions_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `support_budgets`
--
ALTER TABLE `support_budgets`
  ADD CONSTRAINT `support_budgets_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `support_budgets_ibfk_2` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`currency_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `support_budget_expenses`
--
ALTER TABLE `support_budget_expenses`
  ADD CONSTRAINT `support_budget_expenses_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `support_budget_expenses_ibfk_2` FOREIGN KEY (`cost_category_id`) REFERENCES `cost_categories` (`cost_category_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
