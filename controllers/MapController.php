<?php

namespace app\controllers;

use app\models\District;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MapController implements the CRUD actions for DistrictGeoData model.
 */
class MapController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionJson(){
        $data = District::getProjectsMapData();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    public function actionTest(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return District::getMaxProjectCount() + 2;
    }
}
