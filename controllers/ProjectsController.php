<?php

namespace app\controllers;

use app\models\District;
use app\models\FinancingSource;
use app\models\Organization;
use Yii;
use app\models\Project;
use app\models\search\Project as ProjectSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class ProjectsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ], $this->formData()));
    }

    /**
     * Displays a single Project model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionInDistrict($id){
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->searchDistrict(Yii::$app->request->queryParams, $id);

        return $this->render('in-district', ArrayHelper::merge([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'district' => District::findOne($id),
            'districts' => ArrayHelper::map(District::find()->all(), 'district_id', 'district'),
        ], $this->formData()));
    }

    private function formData(){
        return [
            'organisations'=>ArrayHelper::map(Organization::find()->all(), 'organization_id', 'organization_name'),
            'financiers'=>ArrayHelper::map(FinancingSource::find()->all(), 'financing_source_id', 'financing_source'),
        ];
    }
}
